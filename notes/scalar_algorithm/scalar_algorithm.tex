\documentclass[11pt, oneside]{article}   	

\usepackage{jheppub}



\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amsbsy}
\usepackage{array}
\usepackage[linesnumbered,lined,boxed,commentsnumbered]{algorithm2e}

\usepackage{mathtools}
\usepackage[usenames,dvipsnames]{xcolor}

\usepackage{subcaption}

\usepackage{dsdshorthand}


\usepackage[vcentermath]{youngtab}
\newcommand{\myng}[1]{\,{\tiny\yng #1}\,}

\usepackage{tikz}
\usetikzlibrary{trees}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shapes.misc}
 
\tikzset{
  threept/.style={
    circle,
    draw,
    inner sep=2pt,
  },
  twopt/.style={
    circle,
    draw,
    fill=black,
    inner sep=1pt,
    minimum size=1pt
  },
  cross/.style={
    cross out,
    draw=black, 
    minimum size=7pt, 
    inner sep=0pt,
    outer sep=0pt
  },
  scalar/.style={
    thick,
    dashed,
    postaction={
      decorate,
      decoration={
        markings,
        mark=at position 0.5 with {\arrow{>}}
      }
    }
  },
  spinning/.style={
    thick,
    postaction={
      decorate,
      decoration={
        markings,
        mark=at position 0.5 with {\arrow{>}}
      }
    }
  },
  spinning no arrow/.style={
    thick,
  },
  finite with arrow/.style={
    decoration={
      snake,
      amplitude=1pt,
      segment length=6pt,
      post length=2pt
    },
    decorate,
    thick,->
  },
  finite/.style={
    decoration={
      snake,
      amplitude=1pt,
      segment length=6pt,
    },
    decorate,
    thick
  }
}

\newcommand*{\uniq}{\raisebox{-0.7ex}{\scalebox{1.8}{$\cdot$}}}

\newcommand\wL{\mathbf{L}}
\newcommand\wS{\mathbf{S}}
\newcommand\wSD{\mathbf{S}_\De}
\newcommand\wSE{\mathbf{S}_E}
\newcommand\wSJ{\mathbf{S}_J}
\newcommand\wF{\mathbf{F}}
\newcommand\wR{\mathbf{R}}
\newcommand\wRb{\bar{\mathbf{R}}}

\newcommand{\tsym}{\cT}

\newcommand{\PK}[1]{[{\color{red}#1}]}

\makeatletter
\def\@fpheader{\ }
\makeatother

\title{Casimir-based algorithm for scalar conformal blockss}
\author{Petr Kravchuk}
\date{}


\begin{document}

\maketitle

\section{The coefficients}

Scalar conformal blocks for exchange of primary operator with dimension $\De$ and spin $J$ have the series expansion in the form
\be\label{eq:scalarmasterseries}
	G_{\De,J}(s,\f)=\sum_{p=0}^\oo \L_{p,j} s^{\De+p} \hat C^{(\nu)}_j(\cos\f),
\ee
where $z=s e^{i\f}$
\be
	\hat C^{(\nu)}_j(\cos\f)=\frac{ C^{(\nu)}_j(\cos\f)}{C^{(\nu)}_j(1)}.
\ee
The recursion relations take the form
\be\label{eq:scalarmaster}
	(C_{p,j}-C_{0,J})\L_{p,j}=\Gamma^+_{p-1,j-1}\L_{p-1,j-1}+\Gamma^-_{p-1,j+1}\L_{p-1,j+1},
\ee
where~\cite{Hogervorst:2013sma} (see e.g.~\cite{Kravchuk:2017dzd} for $\De_{ij}\neq 0$)
\be
	\Gamma^+_{p,j}&=\frac{(\De+p+j-\De_{12})(\De+p+j+\De_{34})(j+d-2)}{2j+d-2},\nn\\
	\Gamma^-_{p,j}&=\frac{(\De+p-j-d+2-\De_{12})(\De+p-j-d+2+\De_{34})j}{2j+d-2},
\ee
and
\be
	C_{p,j}=(\De+p)(\De+p-d)+j(j+d-2).
\ee

\subsection{Ambiguities in solving the Casimir equation}

The recursion relation discussed above solves the following problem:

\textit{Given the leading term in~\eqref{eq:scalarmasterseries} find the solution of the quadratic Casimir equation which has the form~\eqref{eq:scalarmasterseries}.}

This statement of the problem implicitly assumes that there exists a unique solution. This is indeed true for generic $\De$. However, we will soon see that non-generic $\De$ are important for us, and so we need to be more careful. 

The uniqueness can fail if there are solutions to the Casimir differential equation of the form~\eqref{eq:scalarmasterseries} whose leading term looks like a descendant term in~\eqref{eq:scalarmasterseries}. This only can happen if some descendant $(\De+p,j)$ of the primary $(\De,J)$ has the same Casimir as the primary. If this is the case, then indeed, the left-hand side of~\eqref{eq:scalarmaster} is zero and $\L_{p,j}$ cannot be determined.

If this happens, then there are two distinct scenarios: (a) the right-hand side of~\eqref{eq:scalarmasterseries} is non-zero, and (b) the right-hand side of~\eqref{eq:scalarmasterseries} is zero. In scenario (a) the equation is inconsistent and we conclude that $\De$ is a singular point for the conformal block, and we will study this scenario later. In scenario (b) there is no problem with~\eqref{eq:scalarmasterseries} and $\L_{p,j}$ is simply a free parameter, where the freedom corresponds to the possibility of adding the subleading solution to the Casimir equation described above.

To understand how often we get this problem, it is useful to understand how to find solutions to
\be\label{eq:degenerationequation}
	C_{p,j}=C_{0,J}.
\ee
This equation is equivalent to
\be
	(\De+p)(\De+p-d)+j(j+d-2)=\De(\De-d)+J(J+d-2).
\ee
We define
\be
	&\nu=2\De-d,\quad \mu=2J+d-2,\\
	&\nu'=\nu+2p,\quad \mu'=\mu+2(j-J),
\ee
and the equation becomes
\be
	\nu^2+\mu^2=\nu'^2+\mu'^2.
\ee
We are interested in solutions $\nu'$ and $\mu'$ such that $p=(\nu'-\nu)/2$ is a positive integer, $|\mu'-\nu|/2$ is a non-negative integer no larger than $p$.\footnote{I think we can also add a selection rule that $(\mu'-\mu)/2$ has the same (or opposite) parity as $p$, depending on the parity of the three-point structures in the conformal block.} Below we will also only consider the cases when $\nu$ and $\mu$ are integers. We can recast the equation as
\be
	\nu^2+\mu^2=(\nu'+i\mu')(\nu'-i\mu'),
\ee
so that we have a factorization of $\nu^2+\mu^2$ over Gaussian integers into two complex conjugate factors. All such factorizations can be obtained by looking at Gaussian prime factorization of $\nu+i\mu$. For example, Mathematica handles this easily with

\texttt{FactorInteger[}$\nu+i\mu$\texttt{,GaussianIntegers->True]}

For every $\nu$ and $\mu$ this gives a finite list of factorizations, from which we can then select those which satisfy the required conditions.

If we know how many descendants solve~\eqref{eq:degenerationequation} we can determine how to proceed with the solution of the recursion relations. In particular, if we want to compute conformal block at $\De=\De_0$ and scenario $(a)$ doesn't occur at this $\De$, then we need to solve for coefficients $\L_{p,j}$ as power series in $\De-\De_0$, truncated to order $N$, i.e.
\be\label{eq:bscenarioresolution}
	\L_{p,j}=\sum_{k=0}^N (\De-\De_0)^k\L^{(k)}_{p,j}+O((\De-\De_0)^{N+1}).
\ee
Each time we divide by $0$ in~\eqref{eq:scalarmasterseries}, which now resolves into $\De-\De_0$, we should check that (in the absence of scenario (a)) the right hand side of~\eqref{eq:scalarmasterseries} has vanishing $(\De-\De_0)^0$ term, so we can perform division and loose one order in the expansion~\eqref{eq:bscenarioresolution}. After passing through $N$ descendants which solve~\eqref{eq:degenerationequation} we are guaranteed to still have at least one order left, so we can set $\De=\De_0$.


\subsection{The poles}

The equation~\eqref{eq:scalarmaster} contains the factor
\be\label{eq:casimirdifference}
	C_{p,j}-C_{0,J}&=(\De+p)(\De+p-d)+j(j+d-2)-\De(\De-d)-J(J+d-2)\nn\\
	&=2p \De+j(j+d-2)-J(J+d-2)+p(p-d)
\ee
We get a pole in $\L_{p,j}$ at all dimensions $\De$ where this vanishes, but the right-hand side of~\eqref{eq:scalarmaster} doesn't (scenario (a) above). We know the exhaustive list of poles of the scalar conformal block~\cite{Kos:2014bka}:
\be\label{eq:poles}
	\mathrm{I}:&\quad\De=-J-(k-1),\nn\\
	\mathrm{II}:&\quad\De=\frac{d-2}{2}-(k-1),\nn\\
	\mathrm{III}:&\quad\De=J+d-2-(k-1),
\ee
where $k\geq 1$, and for the third family we have $k\leq J$.

\PK{Actually, is it at all interesting to consider non-integer $J$ in this context? It seems that it should be a relatively minor modification, but I am not sure whether it is actually useful to have numerical approximation of non-integer spin blocks.}

Clearly, most of the time zeros of~\eqref{eq:casimirdifference} do not coincide with any of the families, since these zeros appear at
\be
	\De=\frac{J(J+d-2)-j(j+d-2)-p(p-d)}{2p},
\ee
and there is generically no reason for the denominator to cancel to give (half-)integer. So most of these poles must cancel against the right hand side of~\eqref{eq:scalarmaster}.

The actual poles in scalar blocks appear for
\be\label{eq:nulldescs}
	\begin{matrix*}[l]
	\mathrm{I}:&\quad j=J+k,&\quad p=k,\\
	\mathrm{II}:&\quad j=J,&\quad p=2k,\\
	\mathrm{III}:&\quad j=J-k,&\quad p=k.
	\end{matrix*}
\ee
Even thought the respective values of $\Delta$ do lead to these physical poles, we can still have a bunch of unphysical poles, described by scenario (b) in the previous section. So we must take care when solving the equations as discussed there.

%We can ask whether this is the only way to get $\De$ in a physical family. Simple experimentation in \texttt{Mathematica} shows that it is not. For a given $\De$ in a physical family we can build the list of $(p,j)$ corresponding to all descendants at levels $p\leq p_{max}$ and compute~\eqref{eq:casimirdifference} for these descendants (at some values of $J$ and $k$). Most often one finds only one descendant for which~\eqref{eq:casimirdifference} vanishes, but in some cases there are more than one. 

%If one demands that $j$ differs from $J$ by an integer and requires~\eqref{eq:casimirdifference} vanish as a function of $J$, it is very easy to show that~\eqref{eq:nulldescs} are the only options. However, we would like to work with a numerical value of $J$, which means that for a given physical pole, we will occasionally encounter zero in~\eqref{eq:casimirdifference} more than once. Let us call the would-be double (or higher-order) poles arising from this kind of situations the ``subtle spurious poles.''\footnote{Of course in even dimensions we actually want some second-order poles, and we have to be careful not to mistake them for these subtle spurious poles.} We would have to see how to process these subtle poles numerically. It is probably not too big of a deal. Note that the subtle spurious poles seem to be a feature of integer $J$ only. 

%I think these are probably related to null states inside of Verma submodules of null states. The fact that they go away perhaps is the consequence of the fact that there are no non-trivial compositions of conformally-invariant differential operators in odd dimensions (i.e. like in de Rham complex). In even dimensions double-poles probably come from the fact that we can have non-trivial compositions 

\subsubsection{Which poles to keep}

We need to decide which physical poles to keep in our approximation. I am not sure whether it is useful to give the user full control over the set of poles or to have a single parameter like $\kappa=$\texttt{keptPoleOrder}. 

The latter parameter arises from the observation that poles in families I and III contribute at $\rho^k$ and poles in family II contribute at $\rho^{2k}$. By specifying $\kappa$ we mean that we only include poles with $k\leq \kappa$ from families I and III and $k\leq \kappa/2$ from family II.

\PK{Is this actually the most common definition of \texttt{keptPoleOrder}?}

This discussion works for non-even dimensions. In even dimensions we get double poles. They arise from collision of families. Note that the pole of the III family has $\De\geq d-1$, while the I and II families have $\De\leq \frac{d-2}{2}$, so the collision can only happen between I and II. For this to happen, dimension must be even. In that case we have the condition
\be
	-J-(k_I-1)=\frac{d-2}{2}-(k_{II}-1)
\ee
so that
\be
	k_{II}=k_I+\p{\frac{d-2}{2}+J}.
\ee
This means that
\begin{enumerate}
	\item Every pole of I family collides with some pole of II family.
	\item The order of the II pole is no less than the order of I pole, and most of the time ($d>2$ or $J>0$) is strictly greater.
\end{enumerate} 
Near this pole the conformal block has structure
\be
	G\sim \frac{G^{(-2)}}{(\De-\De_i)^2}+\frac{G^{(-1)}}{\De-\De_i}+O(1)
\ee
Note that if we look at the conformal block at order $\kappa<k_{II}$, we will not see the II family pole, so there will be no collision. It means that $G^{(-2)}$ is order $\rho^{k_{II}}$, and only $G^{(-1)}$ is order $\rho^{k_{I}}$. This is also natural from the Casimir recursion relation.

This discussion implies that if we want to keep only poles at certain $\rho$-order, we need to split some double poles in even dimensions. We should perhaps decide what is the most natural truncation scheme.

%\subsection{Recursion for poles}
%
%The thing that has nice behavior at infinity in $\De$ is 
%\be
%	H_{\De,J}=r^{-\De}G_{\De,J},
%\ee
%where $\rho = r e^{i\f}$. We have the representation,
%\be
%	H_{\De,J}=H_{\oo}+\sum_{i}\frac{H_i^{(1)}}{\De-\De_i}+\frac{H_i^{(2)}}{(\De-\De_i)^2}
%\ee
%where everything in the rhs implicitly depends on $J$, and double-poles are absent in non-even dimensions.
%
%We know the explicit form of $H_\oo$~\cite{Kos:2014bka}. The residues $H_i^{(n)}$ are easy to compute if we know the analogous residues of functions $G$,
%\be
%	G_{\De,J}\sim \frac{G_i^{(-1)}}{\De-\De_i}+\frac{G_i^{(-2)}}{(\De-\De_i)^2}.
%\ee
%We will therefore focus on computing these residues. In fact, we will also factor out $s^{\De}$ and look at the residues $\tl G_i^{(n)}$ of 
%\be
%	\tl G_{\De,J}=s^{-\De} G_{\De,J}.
%\ee
%
%\PK{These choices might affect convergence of the power series, may want to revisit this -- perhaps better to look at residues of $H$. But let's stick to the simplest scheme for now.}
%
%We will in addition also track $\tl G^{(0)}$. These residues are given by
%\be
%	\tl G_i^{(n)}=\sum_{p=0}^\oo \L_{p,j}^{i,(n)} s^{p} \hat C^{(\nu)}_j(\cos\f),	
%\ee
%and we have the following recursion relation for $\L_{p,j}^{i,(n)}$:
%\begin{itemize}
%	\item If~\eqref{eq:casimirdifference} is non-zero for $\De=\De_i$ given by~\eqref{eq:nulldescs}, then
%	\be
%		\L_{p,j}^{i,(-2)}=&\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(-2)}+\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(-2)},\nn\\
%		\L_{p,j}^{i,(-1)}=&\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(-1)}+\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(-1)}\nn\\
%		&+\ptl_\De\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(-2)}+\ptl_\De\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(-2)},\nn\\
%		\L_{p,j}^{i,(0)}=&\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(0)}+\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(0)}\nn\\
%		&+\ptl_\De\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(-1)}+\ptl_\De\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(-1)}\nn\\
%		&+\half\ptl^2_\De\tl\Gamma^+_{p-1,j-1}(\De_i)\L_{p-1,j-1}^{i,(-2)}+\half\ptl^2_\De\tl\Gamma^-_{p-1,j+1}(\De_i)\L_{p-1,j+1}^{i,(-2)},
%	\ee
%	where $\tl\Gamma$ include also the Casimir difference~\eqref{eq:casimirdifference} in denominator.
%	\item If~\eqref{eq:casimirdifference} vanishes for $\De=\De_i$ given by~\eqref{eq:poles} and also~\eqref{eq:nulldescs} is true, then
%	\be
%		\ldots
%	\ee
%\end{itemize}

\section{Implementation details}

Our goal is to use a hybrid approach where we use the following representation for the scalar blocks,
\be
	G_{\De,J}(z,\bar z)=r^{\De} H_{\De,J}(z,\bar z),
\ee
where $r=\sqrt{\rho\bar\rho}$, and $H$ as a function of $\De$ has the representation (we assume odd dimensions for now and $J$ is fixed)
\be
	H_{\De,J}(z,\bar z) = H_{\infty}(z,\bar z)+\sum_{\mathbf{i}} \frac{R_{\mathbf{i}}(z,\bar z)}{\De-\De_i},
\ee
where $H_{\infty}$ is a known function. Here $\mathbf{i}=(\text{type},k)$ labels the physical poles~\eqref{eq:poles}.  We would like to use the recursion relation~\eqref{eq:scalarmaster} to determine the derivatives of the residues $R_\mathbf{i}$ around the crossing-symmetric point.

From the series~\eqref{eq:scalarmasterseries} we see that it is the easiest to determine the residues of $s^{-\De}G$, since in this function $\De$-dependence enters only through the coefficients $\L$, and $s$ enters as $s^n$. For expansion of $H$ the coordinates enter as 
\be\label{eq:hardfactors}
	\p{\frac{s}{r}}^\De s^n\qquad\text{and}\qquad C^{(\nu)}_j(\cos\f).
\ee
It seems to be a good strategy to take derivatives of these quantities and then re-expand then at $\f=0$ in power series in $r$. The fact that these quantities depend on $\De$ is not too problematic -- since eventually we need only the residue, we can simply set $\De=\De_\mathbf{i}$ for the coordinate-dependent factors, in odd dimensions. In even dimensions we need to take into account the existence of a double pole.

There are two competing factors. On the one hand, we want to compute the series which converges the fastest. On the other, we want to compute the series which is the easiest to compute. If we were to compute the power series for $s^{-\De}G$, it would be super-easy to take derivatives with respect to $s$ and $\f$, set $\f=0$ and re-expand in $r$-series. However, experimentation with 1d blocks shows that 
\begin{enumerate}
	\item the $r$-series for $H$ converge faster than for $s^{-\De}G$ (precisely: it appears that exponential convergence of $s^{-\De}G$ gets delayed by approx $\De$ orders of $\rho$, where $\De$ here is as in~\eqref{eq:poles}, while $H$ always converges exponentially starting from the first few orders), and
	\item the $r$-series for $\rho$-derivatives converge faster than those for $z$-derivatives (precisely: it appears that both $\rho$- and $z$-derivatives get the exponential convergence corrected by a polynomial, but the degree of that polynomial is higher in the $z$-derivative case).
\end{enumerate} 

Since the value of $\De$ in~\eqref{eq:poles} can be $\geq 50$, it appears that we want to be computing the series for $H$ and thus we need to take derivatives of~\eqref{eq:hardfactors}. It doesn't appear to be an especially easy task to take derivatives of these objects in any natural set of coordinates, and re-expanding the result in $r$-series seems to be a painful task. Maybe we should simply accept the fact that we'll have to work a bit here, and compute the $\rho,\bar\rho$ derivatives of $H$?

If so, we can probably build up from $n=0$ and iteratively compute $r$-expansions of derivatives of~\eqref{eq:hardfactors} at higher and higher $n$. If we are doing $z,\bar z$- or $\rho,\bar \rho$-derivatives, we need to figure out how these power series behave near $r=0$. (In general these derivatives will introduce negative powers.) 

\subsection{Algorithm}

The first approximation to the algorithm is algorithm~\ref{alg:mainalgo}.
\begin{algorithm}
	\KwIn{$d$ (odd) number of dimensions, spin $J$, $\cP$ list of poles to compute residues of, $N$ $\rho$-order of apporximation, $\cD$ list of derivatives to compute.}
	\KwResult{Rational approximation for each derivative in $\cD$.}
	\ForEach{$P\in \cP$}{
		let $p_P$ and $j_P$ be the level and spin of the null-decendant corresponding to $p$\;
		let $\De_P$ be the position of the pole $P$\;
		let $k$ be the number of descendants of $(\De_P,J)$ with $p<p_P$ and $|j-j_P|\leq p_P-p$ and correct parity of $j-J$ which have the same Casimir as $(\De_P,J)$\;
		initialize $\L_{0,J}$ by power series $1+O\p{(\De-\De_P)^{k+1}}$\;
		\For{$p:\,\, 1\leq p \leq p_P$}{
			\For{$j:\,\,|j-J|\leq p$ and $|j-j_P|\leq p_P$ and $j-J+p$ even}{
				compute $\L_{p,j}$ using algorithm 2.
			}
		}
		let $R$ be the coefficient of $(\De-\De_P)^{-1}$ in $\L_{p_P,j_P}$\; 
		choose a perhaps simpler or better-convergent basis of derivatives $\cD$' equivalent to $\cD$\;
		$D_{j_P}$=$r$-expansion to order $N$ of all derivatives in $\cD'$ of $\p{\frac{s}{r}}^{\De_P}C^{(\nu)}_{j_P}(\cos\f)$ at $\f=0$\;
		initialize a numerical list $\mathfrak{D}'_P$ of derivatives in $\cD'$ with $R (D_{j_P}\vert_{r=\rho_{crossing}})$\;
		let $k$ be the number of descendants of $(\De_P+p_P,j_P)$ with $p\leq N$ and correct parity of $j-J$ which have the same Casimir as $(\De_P+p,j_P)$\;
		initialize $\L_{0,j_P}$ with $R+O((\De-\De_P-p_P)^k)$\;
		\For{$p:\,\, 1\leq p \leq p_P$}{
			\For{$j:\,\,|j-j_P|\leq p$ and $j-j_P+p$ even}{
				compute $D_j$ for $\p{\frac{s}{r}}^{\De_P}s^pC^{(\nu)}_{j_P}(\cos\f)$ using $D_j$ for $p-1$\;
				compute $\L_{p,j}$ using algorithm 2\;
				$\mathfrak{D}'_P\leftarrow \mathfrak{D}'_P+\p{\L_{p,j}\vert_{\De=\De_P+p_P}}(D_j\vert_{r=\rho_{crossing}})$\;
			}
		}
		convert $\mathfrak{D}'_P$ to $\mathfrak{D}_P$ for $\cD$\;
	}
	combine $\mathfrak{D}_P$ with derivatives of $H_{\oo}$ to compute rational approximations of blocks\;
	\caption{Main algorithm}
	\label{alg:mainalgo}
\end{algorithm}

\bibliographystyle{JHEP}
\bibliography{refs}

\end{document}  




