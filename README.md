# conformal-blocks

This project is supposed to give us an efficient tool to generate arbitrary non-SUSY conformal blocks in 3d and 4d. (With possibility of extension to higher-d, at least for some classes of blocks.) 

*Note: there is also a Dropbox folder for this project which potentially contains stuff. Email Petr or Luca to get access.*

The basic plan for the project so far is:

0. Prototype for scalar conformal blocks using the algorithm discussed during the hackathon
  1. Mathematica prototype
  2. C++ implementation
  3. Test C++ implementation. If it is fast enough, go to next step.
1. Implementation for 3d conformal blocks
  1. Mathematica prototype
  2. C++ implementation, this time with a layer of abstraction separating 3d-specific stuff
2. Implementation for 4d conformal blocks
  1. Mathematica prototype (mostly figuring out the conventions for rep. theory)
  2. Extending the exisiting C++ implementation
3. ?????????
4. PROFIT 


Some random notes:

1. If the C++ implementation of scalar blocks turns out to be sufficiently nice, we can consider keeping it separate and having some nice features as general spacetime dimension (including the fractional ones). But of course we should make sure that it works if we set, e.g. d=4.
2. Might be nice to have e.g. Mathematica code for relating code bases to more popular bases of tensor structures
