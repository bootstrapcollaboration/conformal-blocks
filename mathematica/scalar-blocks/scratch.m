(* ::Package:: *)

(*

This is a scratch file to test some things. Here we will probably do stuff mostly symbolically.

*)


(* ::Subchapter:: *)
(*Recursion relation coefficients*)


casimirDifference[\[CapitalDelta]_,J_,p_,j_]:=2*p*\[CapitalDelta]+j(j+d-2)-J(J+d-2)+p(p-d);


GammaPlus[\[CapitalDelta]_,p_,j_]:=(\[CapitalDelta]+p+j-\[CapitalDelta]12)(\[CapitalDelta]+p+j+\[CapitalDelta]34)(j+d-2)/(2j+d-2);
GammaMinus[\[CapitalDelta]_,p_,j_]:=(\[CapitalDelta]+p+2-d-j-\[CapitalDelta]12)(\[CapitalDelta]+p+2-d-j+\[CapitalDelta]34)(j)/(2j+d-2);


GammaTlPlus[\[CapitalDelta]_,J_,p_,j_]:=GammaPlus[\[CapitalDelta],p,j]/casimirDifference[\[CapitalDelta],J,p+1,j+1];
GammaTlMinus[\[CapitalDelta]_,J_,p_,j_]:=GammaMinus[\[CapitalDelta],p,j]/casimirDifference[\[CapitalDelta],J,p+1,j-1];


dGammaTlPlus[\[CapitalDelta]_,J_,p_,j_]=D[GammaTlPlus[\[CapitalDelta],J,p,j],\[CapitalDelta]]//Factor;
dGammaTlMinus[\[CapitalDelta]_,J_,p_,j_]=D[GammaTlMinus[\[CapitalDelta],J,p,j],\[CapitalDelta]]//Factor;


ddGammaTlPlus[\[CapitalDelta]_,J_,p_,j_]=1/2 D[GammaTlPlus[\[CapitalDelta],J,p,j],\[CapitalDelta],\[CapitalDelta]]//Factor;
ddGammaTlMinus[\[CapitalDelta]_,J_,p_,j_]=1/2 D[GammaTlMinus[\[CapitalDelta],J,p,j],\[CapitalDelta],\[CapitalDelta]]//Factor;


pGammaTlPlus[\[CapitalDelta]_,J_,p_,j_]:=1/(2*p) GammaPlus[\[CapitalDelta],p,j];
pd



(* ::Subchapter:: *)
(*Pole families*)


poleDelta[1][J_,k_]:=-J-(k-1);
poleDelta[2][J_,k_]:=(d-2)/2-(k-1);
poleDelta[3][J_,k_]:=J+d-2-(k-1);

poleOrder[1][J_,k_]:=k;
poleOrder[2][J_,k_]:=2k;
poleOrder[3][J_,k_]:=k;

poleSpin[1][J_,k_]:=J+k;
poleSpin[2][J_,k_]:=J;
poleSpin[3][J_,k_]:=J-k;


polesAtOrder[J_,keptPoleOrder_]:=
	Table[pole[1][J,k],{k,1,keptPoleOrder}]~Join~
	Table[pole[2][J,k],{k,1,keptPoleOrder/2}]~Join~
	Table[pole[3][J,k],{k,1,Min[keptPoleOrder,J]}];


(* ::Subchapter:: *)
(*Recursion on coefficients*)


(*

Here we use the following storage convention for poles of \[CapitalLambda][p,j]

\[CapitalLambda][p,j][{laurentLeading[pole[t][J,k],{\[CapitalLambda]^(-2),\[CapitalLambda]^(-1),\[CapitalLambda]^(0)}],...}]

*)


updateLaurentLeading[
		J_,p_,j_,
		laurentLeading[p:pole[t_][J_,k_],cfsMinus_],
		laurentLeading[p:pole[t_][J_,k_],cfsPlus_]
	]:=Module[{\[CapitalDelta]i},
		\[CapitalDelta]i=p/.pole->poleDelta;
		If[Simplify[casimirDifference[\[CapitalDelta]i,J,p,j]]!=0,
			laurentLeading[p,{
				cfsMinus[[1]]GammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[1]]GammaTlMinus[\[CapitalDelta]i,J,p,j],
				
				cfsMinus[[2]]GammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[2]]GammaTlMinus[\[CapitalDelta]i,J,p,j]+
				cfsMinus[[1]]dGammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[1]]dGammaTlMinus[\[CapitalDelta]i,J,p,j],
				
				cfsMinus[[3]]GammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[3]]GammaTlMinus[\[CapitalDelta]i,J,p,j]+
				cfsMinus[[2]]dGammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[2]]dGammaTlMinus[\[CapitalDelta]i,J,p,j]+
				cfsMinus[[1]]ddGammaTlPlus[\[CapitalDelta]i,J,p,j]+
				cfsPlus[[1]]ddGammaTlMinus[\[CapitalDelta]i,J,p,j]
			}],
			
		]
	]
