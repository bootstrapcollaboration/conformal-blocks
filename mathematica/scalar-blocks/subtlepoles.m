(* ::Package:: *)

(* ::Input:: *)
(*quadraticDescendants[d_,\[CapitalDelta]_,J_]:=Module[{\[Nu]=\[CapitalDelta]-d/2,\[Mu]=J+(d-2)/2,integerpoints},*)
(*integerpoints=(2\[Nu]+2I \[Mu])/#1 Conjugate[#1]&/@Divisors[2\[Nu]+2I \[Mu],GaussianIntegers->True];*)
(*integerpoints=DeleteDuplicates[integerpoints~Join~(-integerpoints)~Join~(I integerpoints)~Join~(-I integerpoints)];*)
(*integerpoints={0,J}+#&/@Cases[{(Re@#)/2-\[Nu],(Im@#)/2-\[Mu]}&/@integerpoints,{n_,\[Delta]J_}/;IntegerQ[n]&&IntegerQ[\[Delta]J]&&n>0&&Abs[\[Delta]J]<=n&&\[Delta]J+J>=0];*)
(*Sort[integerpoints]*)
(*]*)


(* ::Input:: *)
(*quadraticDescendantsWithParitySelection[d_,\[CapitalDelta]_,J_]:=Cases[quadraticDescendants[d,\[CapitalDelta],J],{n_,j_}/;EvenQ[n+j-(J)]]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendantsWithParitySelection[3,1-J-k,J]],*)
(*{J,0,200},*)
(*{k,1,100}*)
(*]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendantsWithParitySelection[3,3/2-k,J]],*)
(*{J,0,200},*)
(*{k,1,100}*)
(*]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendantsWithParitySelection[3,J+2-k,J]],*)
(*{J,0,200},*)
(*{k,1,J}*)
(*]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendants[3,1-J-k,J]],*)
(*{J,0,200},*)
(*{k,1,100}*)
(*]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendants[3,3/2-k,J]],*)
(*{J,0,200},*)
(*{k,1,100}*)
(*]*)


(* ::Input:: *)
(*Max@Table[*)
(*Length[quadraticDescendants[3,J+2-k,J]],*)
(*{J,0,200},*)
(*{k,1,J}*)
(*]*)


Histogram[Flatten[Table[
Length[quadraticDescendantsWithParitySelection[3,1-J-k,J]],
{J,0,200},
{k,1,100}
]],PlotRange->Full]


Histogram[Flatten[Table[
Length[quadraticDescendantsWithParitySelection[3,3/2-k,J]],
{J,0,200},
{k,1,100}
]],PlotRange->Full]


Histogram[Flatten[Table[
Length[quadraticDescendantsWithParitySelection[3,J+2-k,J]],
{J,0,200},
{k,1,100}
]],PlotRange->Full]
